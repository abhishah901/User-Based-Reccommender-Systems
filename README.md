# User-Based-Reccommender-Systems
A primitive implementation of the User Based Recommender System in Python

To install python packages, you can use any python package managment software, such as pip, conda. For example, in pip, you could type pip install nose in the terminal to install the package.

This might not be the best solution to the problem but a plausible solution. Points are used to signify the complexity of the function and understand the building blocks of the algorithm.

The UBaseReco and testUBaseReco works with the famous movielens dataset from the train and test csv files and uses the data to implement User Based Recommendation Systems and checks the accuracy with RMSE.

Make sure the csv files are in the right directories.
